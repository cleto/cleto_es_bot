import random
import requests

SPANISH_JOKES_URL = 'https://raw.githubusercontent.com/jleahred/ortograph/master/data/chistes%20cortos.txt'

NERD_JOKES_URL = 'https://raw.githubusercontent.com/pjimenezmateo/Computer-jokes/master/jokes.txt'


def random_joke():
    spanish = requests.get(SPANISH_JOKES_URL)
    jokes = []
    if spanish.ok:
        jokes += [
            l[2:].decode('utf-8')
            for l in spanish.content.splitlines() if l
        ]

    nerds = requests.get(NERD_JOKES_URL)
    if nerds.ok:
        jokes += [l.decode('utf-8') for l in nerds.content.splitlines()]
    if jokes:
        return random.choice(jokes)
