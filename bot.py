#!/usr/bin/env python3

import json
import logging
import argparse
from telegram.ext import Updater
from telegram.ext import CommandHandler

from jokes import random_joke

def start(bot, update):
    msg = 'Welcome to *cleto.es*!\n\n'
    joke = random_joke()
    if joke:
        msg += '_{}_\n\n'.format(joke)
    msg += 'Use /help to get the command list'
    bot.send_message(
        chat_id=update.message.chat_id, text=msg, parse_mode='Markdown'
    )


def stop(bot, update):
    bot.send_message(chat_id=update.message.chat_id, text='Bye! ;-)')


def show_help(bot, update):
    bot.send_message(chat_id=update.message.chat_id, text='TBD')


def error(bot, update, error):
    logging.warn('Update "%s" caused error "%s"' % (update, error))


def main(cfg):
    updater = Updater(token=cfg['token'])
    dp = updater.dispatcher
    dp.add_handler(CommandHandler('start', start))
    dp.add_handler(CommandHandler('stop', stop))
    dp.add_handler(CommandHandler('help', show_help))
    dp.add_error_handler(error)

    updater.start_polling()
    updater.idle()
    return 0

if __name__ == '__main__':
    logging.basicConfig(
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        level=logging.INFO
    )
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-c', '--config', default='bot.json',
        help='path to config file',
    )
    args = parser.parse_args()
    exit(main(json.loads(open(args.config).read())))
